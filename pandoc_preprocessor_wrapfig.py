#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import re
import csv
import argparse


class wrapfig():
    """ Allows a more markdown-ish approach to inserting figures with text 
        wrapping and control over width and captioning.
        
        This generates latex output (although other outputs could be easily 
        adapted by editing the template variable), and requires the `wrapfig`, 
        `graphicx` and `caption` latex packages. 
        
        The md format was taken from remark (https://github.com/gnab/remark)
        
        To use, specify figures in your markdown like this:
    
            ![:wrapfig side=side, width=width, ref=ref, caption=caption](path)
    
        Where the parameters can optionally be enclosed in double-quotes, and 
        all are optional except `path`. The are:
        
            path: the path to the image
            
            side: The side of the page to insert the image. Can be "L" or "R"
            
            width: The width of the image, as a proportion of the page (eg., 0.3)
            
            ref: A reference to use to refer to the image (eg., fig:SomeImage1)
            
            caption: A caption (eg., Mean data showing the effect of interest)
        
        Example:
        
        ![:wrapfig side="L", width=0.4, ref="fig:Ref1", caption="A caption."]("path/to/image.pdf")
    
    """

    template = r"""\begin{wrapfigure}{$side}{$width\textwidth}\centering\includegraphics[width=$width\textwidth]{$path}
    \caption{\label{$reference}$caption}\end{wrapfigure}
    """
    
    variables = {"path": "", "side": "L", "width": "1.0", "reference": "", "caption": ""}
    
    re_wrapfig = re.compile('\!\[\:wrapfig[ ]?(.*?)\]\([\"\']{0,1}(.*?)[\"\']{0,1}\)')
    
    def process ( self, data ):

        for m in self.re_wrapfig.finditer(data):

            # Get new vars
            t = self.template
            variables = dict(self.variables)

            # Extract vals from input text block
            variables['path'] = m.group(2).strip(" \"\'")
            for v in csv.reader([m.group(1)], quotechar='"', delimiter=',', skipinitialspace=True):
                vals = v

            for v in vals:
                var,val = v.split("=")
                var = var.strip(" \"\'")
                val = val.strip(" \"\'")
                if variables.has_key(var):
                    variables[var] = val
                
            # Insert vals into output text block
            for var,val in variables.iteritems():
                t = t.replace("${}".format(var), val)
        
            # Insert output text block into output text
            data = data.replace(m.group(0), t)
            
        return data
        

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Simple pandoc preprocessor script to process wrapfig images")
    parser.add_argument("source_file", type=str,
                        help="the path to the source md file")
    parser.add_argument("-o", "--output", default=None, type=str,
                        help="the path to the output md file. If ommitted, write to stdout")
    args = parser.parse_args()

    source_file = args.source_file
    out_file = args.output

    f = open(source_file, 'r')
    data = f.read()
    f.close()

    preproc = wrapfig()

    new_data = preproc.process( data )

    if out_file:
        with open(out_file, "w") as f:
            f.write("%s" % new_data)
            f.flush()
    else:
        print(new_data)
