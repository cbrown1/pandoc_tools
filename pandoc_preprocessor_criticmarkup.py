#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import re
import argparse

class critic():
    
    re_substitution = re.compile('{\~\~(.*?)~>(.*?)\~\~}')
    re_addition = re.compile('{\+\+(.*?)\+\+}')
    re_deletion = re.compile('{\-\-(.*?)\-\-}')
    re_highlight = re.compile('{\=\=(.*?)\=\=}')
    re_comment = re.compile('{\>\>(.*?)\<\<}')
    re_comment_attribution = re.compile('^\@(\S*)')
    
    class addition():
        orig = ""
        val = ""
        start = None
        end = None
    
    class deletion():
        orig = ""
        val = ""
        start = None
        end = None
    
    class substitution():
        orig = ""
        val = ""
        old = ""
        start = None
        end = None
    
    class highlight():
        orig = ""
        val = ""
        start = None
        end = None
    
    class comment():
        orig = ""
        val = ""
        start = None
        end = None
        author = None
    
    comments = []
    additions = []
    deletions = []
    substitutions = []
    highlights = []
    
    show_highlights = False
    show_comments = False
    
    format_substitution = None
    format_addition = None
    format_comment = None
    format_highlight = None

    format_strvar = "$str$"
    
    def process(self, data, version='revised'):
    
        for highlight in self.highlights:
            # TODO: this isn't correct. Highlights should always be shown
            # the question is whether they show as a highlight or just text
            if not self.show_highlights:
                val = ""
            elif self.format_highlight:
                val = self.format_highlight.replace(format_strvar, highlight.val)
            else:
                val = highlight.val
            data = data.replace(highlight.orig, val)

        for comment in self.comments:
            if not self.show_comments:
                val = ""
            elif self.format_comment:
                val = self.format_comment.replace(format_strvar, comment.orig)
            else:
                val = comment.orig
            data = data.replace(comment.orig, val)

        if version == 'revised': 
            for addition in self.additions:
                if self.format_addition:
                    val = self.format_addition.replace(format_strvar, addition.val)
                else:
                    val = addition.val
                data = data.replace(addition.orig, val)
            for deletion in self.deletions:
                data = data.replace(deletion.orig, "")
            for substitution in self.substitutions:
                if self.format_substitution:
                    val = self.format_substitution.replace(format_strvar, substitution.val)
                else:
                    val = substitution.val
                data = data.replace(substitution.orig, val)
    
        elif version == 'original':
            for addition in self.additions:
                data = data.replace(addition.orig, "")
            for deletion in self.deletions:
                data = data.replace(deletion.orig, deletion.val)
            for substitution in self.substitutions:
                data = data.replace(substitution.orig, substitution.old)
        
        else:
            pass
    
        return data
    
    
    def find(self, data):
        
        for m in self.re_substitution.finditer(data):
            this = self.substitution()
            this.start = m.start(0)
            this.end = m.end(0)
            this.orig = m.group(0)
            this.old = m.group(1)
            this.val = m.group(2)
            self.substitutions.append(this)
            
        for m in self.re_addition.finditer(data):
            this = self.addition()
            this.start = m.start(0)
            this.end = m.end(0)
            this.orig = m.group(0)
            this.val = m.group(1)
            self.additions.append(this)
            
        for m in self.re_deletion.finditer(data):
            this = self.deletion()
            this.start = m.start(0)
            this.end = m.end(0)
            this.orig = m.group(0)
            this.val = m.group(1)
            self.deletions.append(this)
            
        for m in self.re_highlight.finditer(data):
            this = self.highlight()
            this.start = m.start(0)
            this.end = m.end(0)
            this.orig = m.group(0)
            this.val = m.group(1)
            self.highlights.append(this)
            
        for m in self.re_comment.finditer(data):
            this = self.comment()
            if m.group(1).strip().startswith("@"):
                ma = self.re_comment_attribution.match(m.group(1))
                this.author = ma.group(0)
                this.val = m.group(1).replace(this.author, "").strip()
            else:
                this.val = m.group(1).strip()
            this.start = m.start(0)
            this.end = m.end(0)
            this.orig = m.group(0)
            self.comments.append(this)


if __name__ == "__main__":

    preproc = critic()

    parser = argparse.ArgumentParser(description = "Simple pandoc preprocessor script to process criticmarkup edits")
    parser.add_argument("source_file", type=str,
                        help="the path to the source md file")
    parser.add_argument("-o", "--output", default=None, type=str,
                        help="the path to the output md file. If ommitted, write to stdout")
    parser.add_argument("-v", "--version", default="revised", 
                        help="What version to output. Can be 'revised' [default] or 'original'")
    parser.add_argument("-a", "--format_add", default=None, 
                        help="A format string for additions. Use {} for the text. EG., \\textcolor{{green}}{{{}}}".format(preproc.format_strvar, preproc.format_strvar))
    parser.add_argument("-c", "--format_comment", default=None, 
                        help="A format string for comments. Use {} for the text. EG., \\textcolor{{red}}{{{}}}".format(preproc.format_strvar, preproc.format_strvar))
    parser.add_argument("-s", "--format_substitution", default=None, 
                        help="A format string for substitutions. Use {} for the text. EG., \\textcolor{{blue}}{{{}}}".format(preproc.format_strvar, preproc.format_strvar))
    parser.add_argument("-l", "--format_highlight", default=None,
                        help="A format string for highlights. Use {} for the text. EG., \\textcolor{{yellow}}{{{}}}".format(preproc.format_strvar, preproc.format_strvar))
    args = parser.parse_args()

    source_file = args.source_file
    out_file = args.output
    version = args.version

    f = open(source_file, 'r')
    data = f.read()
    f.close()

    preproc.format_addition = args.format_add
    preproc.format_comment = args.format_comment
    preproc.format_highlight = args.format_highlight
    preproc.format_substitution = args.format_substitution
    if args.format_comment:
        preproc.show_comments = True

    preproc.find(data)
    new_data = preproc.process(data, version)
    if out_file:
        with open(out_file, "w") as f:
            f.write("%s" % new_data)
            f.flush()
    else:
        print(new_data)
        
#    print("{:} additions".format(len(preproc.additions)))
#    print("{:} deletions".format(len(preproc.deletions)))
#    print("{:} substitutions".format(len(preproc.substitutions)))
#    print("{:} highlights".format(len(preproc.highlights)))
#    print("{:} comments".format(len(preproc.comments)))
#    for c in preproc.comments:
#        print("  {}: {}".format(c.author, c.val))
