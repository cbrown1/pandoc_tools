---
header-includes:
    - \usepackage{graphicx}
    - \usepackage{wrapfig}
    - \usepackage[figurename=Fig.,labelfont={it}]{caption}
---
Here is some \textcolor{red}{added }text. There is also an addition \textcolor{red}{that now contains a fair amount of text}. This is a deletion. And here is a really bad substitution, which is illustrated in Fig. \ref{fig:Ref1}. All of this is actually true [@ieee_ieee_1969].

![:wrapfig side="L", width=0.4, reference="fig:Ref1", caption="A caption."]("src/Fig1.pdf")

![:wrapfig]("src/Fig1.pdf")

