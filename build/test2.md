---
header-includes:
    - \usepackage{graphicx}
    - \usepackage{wrapfig}
    - \usepackage[figurename=Fig.,labelfont={it}]{caption}
---
Here is some \textcolor{red}{added }text. There is also an addition \textcolor{red}{that now contains a fair amount of text}. This is a deletion. And here is a really bad substitution, which is illustrated in Fig. \ref{fig:Ref1}. All of this is actually true [@ieee_ieee_1969].

\begin{wrapfigure}{L}{0.4\textwidth}\centering\includegraphics[width=0.4\textwidth]{src/Fig1.pdf}
    \caption{\label{fig:Ref1}A caption.}\end{wrapfigure}
    

\begin{wrapfigure}{L}{1.0\textwidth}\centering\includegraphics[width=1.0\textwidth]{src/Fig1.pdf}
    \caption{\label{}}\end{wrapfigure}
    

\pagebreak
\pagebreak
XXX_DELIM_XXX
\pagebreak
