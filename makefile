test_preprocessors: src/test.md
	mkdir -p build
	mkdir -p out
	python pandoc_preprocessor_criticmarkup.py --version=revised --format_add="\textcolor{red}{**}" -o build/test1.md src/test.md
	python pandoc_preprocessor_wrapfig.py -o build/test2.md build/test1.md
	pandoc -o out/test.pdf build/test2.md

test_ref_split: src/test_refs.md src/refs.bib
	mkdir -p build
	mkdir -p out
	cp src/test_refs.md build/test_refs.md
	echo "\pagebreak" >> build/test_refs.md
	echo "XXX_DELIM_XXX" >> build/test_refs.md
	echo "\pagebreak" >> build/test_refs.md
	pandoc --bibliography src/refs.bib -o build/test_refs.pdf build/test_refs.md
	./pandoc_ref_split.sh build/test_refs.pdf XXX_DELIM_XXX out/test_refs_Body.pdf out/test_refs_refs.pdf

clean:
	-rm -rf build
	-rm -rf out

install:
	install -D pandoc_preprocessor_criticmarkup.py /usr/local/bin/pandoc_preprocessor_criticmarkup.py
	install -D pandoc_preprocessor_wrapfig.py /usr/local/bin/pandoc_preprocessor_wrapfig.py
	install -D pandoc_ref_split.sh /usr/local/bin/pandoc_ref_split.sh
	ln -s /usr/local/bin/pandoc_preprocessor_criticmarkup.py /usr/local/bin/pandoc_preprocessor_criticmarkup.py
	ln -s /usr/local/bin/pandoc_preprocessor_wrapfig /usr/local/bin/pandoc_preprocessor_wrapfig.py
	ls -s /usr/local/bin/pandoc_ref_split /usr/local/bin/pandoc_ref_split.sh

uninstall:
	-rm /usr/local/bin/pandoc_preprocessor_criticmarkup
	-rm /usr/local/bin/pandoc_preprocessor_wrapfig
	-rm /usr/local/bin/pandoc_ref_split
	-rm /usr/local/bin/pandoc_preprocessor_criticmarkup.py
	-rm /usr/local/bin/pandoc_preprocessor_wrapfig.py
	-rm /usr/local/bin/pandoc_ref_split.sh

