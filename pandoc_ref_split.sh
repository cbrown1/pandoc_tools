# This script allows you to generate a text body with proper citations but 
# without a reference section and a reference section without the text body. 
# This is useful for NIH grants, which require uploading each as separate files. 
# Use caution if you are using page numbers, which will not be correct for the 
# references pdf (they will be one greater than correct).
# 
# 1) Add a unique text delimiter surrounded by page breaks at the end of markdown file
# 
#	echo "\pagebreak" >> test_refs.md
#	echo "XXX_DELIM_XXX" >> test_refs.md
#	echo "\pagebreak" >> test_refs.md
#   echo "# References" >> test_refs.md
# 
# 2) Use pandoc to create a pdf with refs from this markdown file
# 3) Then pass the pdf path, the delimiter used, and the desired names of the 
#    body and ref pdfs to this script

SOURCE=$1
DELIM=$2
OUTBODY=$3
OUTREFS=$4

# Get page number of the delimiter
P_DELIM=$(pdfgrep -n -m 1 "$DELIM" "$SOURCE" | grep -Eo '^[0-9]{1,4}')
# Body ends on previous page
P_BODY_END=$((P_DELIM-1))
# Refs start on next page
P_REFS_START=$((P_DELIM+1))
# Refs end on last page
P_REFS_END=$(gs -q -dNODISPLAY -c "($SOURCE) (r) file runpdfbegin pdfpagecount = quit")

#echo "Delim: $P_DELIM"
#echo "Body: 1 : $P_BODY_END"
#echo "REFS: $P_REFS_START : $P_REFS_END"

# Use gs to split pdf using aquired page numbers
yes | gs -dBATCH -sOutputFile="$OUTBODY" -dFirstPage=1 -dLastPage=$P_BODY_END -sDEVICE=pdfwrite "$SOURCE" >& /dev/null
yes | gs -dBATCH -sOutputFile="$OUTREFS" -dFirstPage=$P_REFS_START -dLastPage=$P_REFS_END -sDEVICE=pdfwrite "$SOURCE" >& /dev/null

